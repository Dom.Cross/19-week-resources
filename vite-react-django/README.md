# BoilerPlate

---

### GitLab

Navigate to `https://gitlab.com/projects/new`

Select `Create Blank Project`

Configure Settings and Select `Create Project`

Select `Clone`

Select `Clone with HTTPS`

In Terminal `cd <your_directory>`

In Terminal `git clone https://gitlab.com/<your_username>/<project_name>.git`

In Terminal `cd <project_name>`

In Terminal `code .`

---

### Django

In Terminal `python -m venv ./.venv`

In Terminal `source ./.venv/bin/activate`

In Terminal `pip install --upgrade pip`

In Terminal `pip install django`

In Terminal `deactivate`

In Terminal `source ./.venv/bin/activate`

In Terminal `pip freeze > requirements.txt`

In Terminal `mkdir django-backend`

In Terminal `mkdir django-backend/api`

In Terminal `mv requirements.txt django-backend/api`

In Terminal `cd django-backend/api`

In Terminal `django-admin startproject backend_project .`

In Terminal `python manage.py startapp backend_rest`

In settings.py

```python
INSTALLED_APPS = [
	  'backend_rest.apps.BackendRestConfig',
		...
]
```

In Terminal `python manage.py migrate`

In settings.py

```python
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': BASE_DIR / 'data/db.sqlite3',
    }
}
```

In Terminal `mkdir data`

In Terminal `mv db.sqlite3 data/`

In Terminal `python manage.py createsuperuser`

In Terminal create username and password, select `y`

In Terminal `python manage.py runserver`

Confirm Install Successful at [`http://localhost:8000/`](http://localhost:8000/)

On Keyboard `CTRL + C`

Create Dockerfile.dev and .gitignore in django-backend/api

In Terminal `docker volume create django-backend-db`

In Terminal `docker build -f Dockerfile.dev . -t django-backend-dev`

In Terminal `docker run -v "$(pwd):/app" -p 8000:8000 django-backend-dev`

Confirm Install Successful at [`http://localhost:8000/`](http://localhost:8000/)

On Keyboard `CTRL + C`

In Terminal `cd ../..`

---

### Vite + React

In Terminal `npm create vite@latest`

In Terminal `react-frontend`

Select `React`

Select `Javascript + SWC`

In Terminal `cd react-frontend`

In Terminal `npm install`

In Terminal `npm run dev`

Confirm Install Successful at [`http://localhost:5173/`](http://localhost:5173/)

On Keyboard `CTRL + C`

In vite.config.js

```jsx
export default defineConfig({
  plugins: [react()],
  server: {
    watch: {
      usePolling: true,
    },
    host: true,
    strictPort: true,
    port: 5173,
  }
})
```

Create run.sh and Dockerfile in react-frontend.

In Terminal `docker build -t react-frontend-image .`

In Terminal `docker run -d --rm -p 5173:5173 react-frontend-image`

Confirm Install Successful at [`http://localhost:5173/`](http://localhost:5173/)

On Keyboard `CTRL + C`

In Docker Select `Stop`

In Terminal `cd ..`

---

### Docker Compose

Create docker-compose.yml, .gitignore, and .gitattributes in top level directory.

In Terminal `docker compose build`

In Terminal `docker compose up`

Confirm Install Successful at [`http://localhost:8000/`](http://localhost:8000/)

Confirm Install Successful at [`http://localhost:5173/`](http://localhost:5173/)

On Keyboard `CTRL + C`

---

### Frontend Toolkit

In Terminal `cd react-frontend`

In Terminal `npm i react-router-dom`

In Terminal `npm i bootstrap@5.3.1`

In Terminal `npm install react-bootstrap bootstrap`

In Terminal `npm install @mui/material @emotion/react @emotion/styled`

In Terminal `npm install @mui/icons-material`

In Terminal `cd ..`

---

### Finish!

In Docker `Delete All Containers`

In Docker `Delete All Images`

In Terminal `deactivate`

In Terminal `rm -r .venv`

In Terminal `docker compose build`

In Terminal `docker compose up`

Confirm Install Successful at [`http://localhost:8000/`](http://localhost:8000/)

Confirm Install Successful at [`http://localhost:5173/`](http://localhost:5173/)

On Keyboard `CTRL + C`

---

### Importing Compiled CSS

You may use [Bootstrap’s ready-to-use css](https://getbootstrap.com/docs/4.0/getting-started/webpack/#importing-compiled-css) by adding this line to your project’s entry point:

`import 'bootstrap/dist/css/bootstrap.min.css';`

---
