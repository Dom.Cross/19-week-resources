# Commit Guidelines

---

### **Commit Message Format**

Each commit message consists of a **header**, a **body** and a **footer**. The header has a special format that includes a **type**, a **scope** and a **subject**:

```
<type>(optional scope): <description>

<optional body>

<optional BREAKING CHANGE>:
<optional breaking change details>

<optional footer(s)>
```

```
fix: fix foo to enable bar

This fixes the broken behavior of the component by doing xyz.

BREAKING CHANGE:
Before this fix foo wasn't enabled at all, behavior changes from <old> to <new>.

Closes D2IQ-12345
```

The **header** is mandatory and the **scope** of the header is optional.

Any line of the commit message cannot be longer 100 characters! This allows the message to be easier to read on GitHub as well as in various git tools.

### **Revert**

If the commit reverts a previous commit, it should begin with `revert:`, followed by the header of the reverted commit. In the body it should say: `This reverts commit <hash>.`, where the hash is the SHA of the commit being reverted.

### **Type**

Must be one of the following:

- `feat` – a new feature is introduced with the changes
- `fix` – a bug fix has occurred
- `chore` – changes that do not relate to a fix or feature and don't modify src or test files (for example updating dependencies)
- `refactor` – refactored code that neither fixes a bug nor adds a feature
- `docs` – updates to documentation such as a the README or other markdown files
- `style` – changes that do not affect the meaning of the code, likely related to code formatting such as white-space, missing semi-colons, and so on.
- `test` – including new or correcting previous tests
- `perf` – performance improvements
- `ci` – continuous integration related
- `build` – changes that affect the build system or external dependencies
- `revert` – reverts a previous commit

### **Scope**

The scope could be anything specifying place of the commit change. For example `$location`, `$browser`, `$compile`, `$rootScope`, `ngHref`, `ngClick`, `ngView`, etc…

### **Subject**

The subject contains succinct description of the change:

- use the imperative, present tense: “change” not “changed” nor “changes”
- don’t capitalize first letter
- no dot (.) at the end

### **Body**

Just as in the **subject**, use the imperative, present tense: “change” not “changed” nor “changes”. The body should include the motivation for the change and contrast this with previous behavior.

### **Footer**

The footer should contain any information about **Breaking Changes** and is also the place to reference GitHub issues that this commit **Closes**.

**Breaking Changes** should start with the word `BREAKING CHANGE:` with a space or two newlines. The rest of the commit message is then used for this.

Can link the JIRA story that would be closed with these changes for example: `Closes D2IQ-<JIRA #>` .

### **Change Default Git Editor to VS Code**

- Open **command terminal**
- Check **Git** is available using the command `git --version`
- Once it is confirmed, use the given command to change the editor
- `git config --global core.editor "code --wait"`
- **or**
- `git config --global core.editor "code -w"`
- After changing the default GIt editor to Visual Studio code, let’s confirm it.
- On your command terminal run:
- `git config --global -e`
- The above command will open the Gitconfig file in the VScode editor.
- Now when you run `git commit` or `git -config --global -e` it will open the Git editor within a file in VS Code.

---

### Resources

[Git Docs](https://docs.github.com/en/get-started/using-git/about-git)

[European Commission](https://ec.europa.eu/component-library/v1.14.2/ec/docs/conventions/git/)

[freeCodeCamp](https://www.freecodecamp.org/news/how-to-write-better-git-commit-messages/)

[HashNode](https://hashnode.com/post/which-commit-message-convention-do-you-use-at-work-ck3e4jbdd00zyo4s1h7mc7e0g)

---

### README

[Make a README](https://www.makeareadme.com/)

[README Template](https://github.com/othneildrew/Best-README-Template)

[freeCodeCamp](https://www.freecodecamp.org/news/how-to-write-a-good-readme-file/)

[WriteTheDocs](https://www.writethedocs.org/guide/writing/beginners-guide-to-docs/)

---

### Design

[Logo Maker](https://www.freelogodesign.org/)

[Shields.io](https://shields.io/)

[Simple Icons](https://simpleicons.org/)

```jsx
* [![Django][Djangoproject.com]][Django-url]

[Djangoproject.com]: https://img.shields.io/badge/Django-092E20?style=for-the-badge&logo=django&logoColor=white
[Django-url]: https://www.djangoproject.com/
```

---
