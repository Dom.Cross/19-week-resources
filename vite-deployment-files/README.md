# Vite Deployment Files

---

### Vite Environment

[Update ENVs:](https://vitejs.dev/guide/env-and-mode.html#env-variables) CTRL + F “process” ⇒ import.meta

[Update URLs:](https://vitejs.dev/guide/env-and-mode.html#env-files) CTRL + F “NAME_URL ⇒ VITE_NAME_URL

### [vite.config.js](https://vitejs.dev/guide/build.html#public-base-path)

```jsx
import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react()],
  server: {
    watch: {
      usePolling: true,
    },
    host: true,
    strictPort: true,
    port: 3000,
  },
  base: "/<project name>",
})
```

### [General .gitlab-ci.yml](https://vitejs.dev/guide/static-deploy.html#gitlab-pages-and-gitlab-ci)

```yaml
image: node:16.5.0
pages:
  stage: deploy
  cache:
    key:
      files:
        - package-lock.json
      prefix: npm
    paths:
      - node_modules/
  script:
    - npm install
    - npm run build
    - cp -a dist/. public/
  artifacts:
    paths:
      - public
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
```

### Project Gamma .gitlab-ci.yml
```yaml
build-front-end-job:
  image: node:16.5.0
  variables:
    CORS_HOST: https://<group_name>.gitlab.io/<repo_name>
    VITE_PUBLIC_URL: https://<group_name>.gitlab.io/<repo_name>
    VITE_REACT_APP_API_HOST: https://<team_name>.mod3projects.com
  stage: build
  cache:
    key:
      files:
        - package-lock.json
      prefix: npm
    paths:
      - ghi/node_modules/
  script:
    - cd ghi
    - npm install
    - npm run build
    - cp dist/index.html dist/404.html
  artifacts:
    paths:
      - ghi/dist/
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
```

---
